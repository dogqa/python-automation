from base.base_page import BasePage
import utilities.custom_logger as cl
import logging


class HomePhpTravels(BasePage):
    log = cl.customLogger(logging.DEBUG)

    def __init__(self, driver):
        super().__init__(driver)#self.driver = driver

    _account = "//*[@class ='dropdown dropdown-login dropdown-tab']"
    _login_link = "//a[@ class ='dropdown-item active tr']"
    _boats = "/html/body/div[2]/div[1]/div[1]/div[3]/div/div/div/div/div/nav/ul/li[3]/a"
    _destination_boats = "/html/body/div[2]/div[1]/div[1]/div[3]/div/div/div/div/div/div/div[3]/div/div/form/div/div/div[1]/div/div[2]/div/a/span[1]"
    _option_destination_boast = "/html/body/div[6]/ul/li[1]/ul/li[1]/div"
    _boats_type = "/html/body/div[2]/div[1]/div[1]/div[3]/div/div/div/div/div/div/div[3]/div/div/form/div/div/div[2]/div/div/div/div/div[2]/div/a/span"
    _option_boats_type = "/html/body/div[2]/div[1]/div[1]/div[3]/div/div/div/div/div/div/div[3]/div/div/form/div/div/div[2]/div/div/div/div/div[2]/div/div/ul/li[2]"
    _date_boats = "/html/body/div[2]/div[1]/div[1]/div[3]/div/div/div/div/div/div/div[3]/div/div/form/div/div/div[3]/div/div/div/div/div/div/div[1]/div/div[2]"
    _cant_adults_boats = "//*[@id='boats']/div/div/form/div/div/div[3]/div/div/div/div/div/div/div[2]/div/div[2]/div/span/button[1]"
    _button_search_boats = "//*[@id='boats']/div/div/form/div/div/div[4]/button"

    def login(self):
        self.elementClick(self._account, locatorType="xpath")
        self.elementClick(self._login_link, locatorType="xpath")

    def boats_action(self):
        self.elementClick(self._boats, locatorType="xpath")
        self.elementClick(self._destination_boats, locatorType="xpath")
        self.elementClick(self._option_destination_boast, locatorType="xpath")
        self.elementClick(self._boats_type, locatorType="xpath")
        self.elementClick(self._option_boats_type, locatorType="xpath")
        self.elementClick(self._date_boats, locatorType="xpath")
        self.elementClick(self._cant_adults_boats, locatorType="xpath")
        self.elementClick(self._button_search_boats, locatorType="xpath")
        return self.driver.current_url