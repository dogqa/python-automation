from pages.home_phptravels import HomePhpTravels as HPT
import unittest
import pytest
import utilities.custom_logger as cl
import logging
from base.webdriverfactory import WebDriverFactory as wd
from datetime import date


@pytest.mark.usefixtures()
class HomeTest(unittest.TestCase):
    log = cl.customLogger(logging.DEBUG)
    driver = wd("chrome")
    _url = "https://www.phptravels.net/"

    @pytest.fixture(autouse=True)
    def objectSetup(self):
        self.driver =self.driver.getWebDriverInstance(self._url)
        self.hp = HPT(self.driver)

    #@pytest.mark.run(order=1)
    def test_validBoats(self):
        today = date.today()
        today_url = today.strftime("%d/%m/%Y")
        BoatsUrl = "https://www.phptravels.net/boats/hong-kong/hong-kong/Motor-boat-Custom-Build-Luxury---2008-refit-2008-?date="+today_url+"&adults=1"
        self.log.info("*#" * 20)
        self.log.info("test_validBoats")
        self.log.info("*#" * 20)
        assert (self.hp.boats_action() == BoatsUrl)